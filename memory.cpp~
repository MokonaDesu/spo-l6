#include <stdlib.h>

#include "memory.h"

#define VIRTUAL_MEMORY_SIZE 256
#define MAX_ALLOCATIONS 64

//Auxilliary stuff
struct AllocationInfo {
  int index;
  int allocationSize;
};

char memory[VIRTUAL_MEMORY_SIZE];
AllocationInfo allocations[MAX_ALLOCATIONS];

int getAllocationInfoByByteIndex(int targetIndex) {
 for (int i = 0; i < MAX_ALLOCATIONS; i++) {
    if (allocations[i].index == targetIndex) {
      return i;
    }
  }
  return -1;
}

int getAllocationSize(int targetIndex) {
  int allocationIndex = getAllocationInfoByByteIndex(targetIndex);
  return allocationIndex == -1 ? 0 : allocations[allocationIndex].allocationSize;
}

int createAllocationAtIndex(int index, int size) {
  for (int i = 0; i < MAX_ALLOCATIONS; i++) {
    if (allocations[i].allocationSize == 0) {
      allocations[i].allocationSize = size;
      allocations[i].index = index;
      return 0;
    }
  }
  return -1;
}

int freeAllocationFromIndex(int index) {
  int allocationIndex = getAllocationInfoByByteIndex(index);
  if (allocationIndex == -1) return -1;

  allocations[allocationIndex].index = 0;
  allocations[allocationIndex].allocationSize = 0;
  return 0;
}

int findFreeChunkIndex(int requiredChunkSize) {
  int currentChunkSize = 0;
  int currentPointer = 0;
  
  while(true) {
    if (currentPointer == VIRTUAL_MEMORY_SIZE) {
      return -1;
    }

    if (currentChunkSize == requiredChunkSize) {
      return currentPointer - requiredChunkSize;
    }

    int currentAllocation = getAllocationSize(currentPointer);
    if (currentAllocation) {
      currentPointer += currentAllocation;
      currentChunkSize = 0;
    } else {
      currentPointer++;
      currentChunkSize++;
    }
  }
}

int min(int x, int y) {
  return x > y ? y : x;
}

//API Methods:
void zeroOut(void* what, int amount) {
  amount--;
  while (amount >= 0) {
    *((char*)what + amount - 1) = 0;
    amount--;
  }
}

void* allocate(int amount) {  
  int freeChunkIndex = findFreeChunkIndex(amount);
  
  if (freeChunkIndex != -1) {
    if (createAllocationAtIndex(freeChunkIndex, amount) != -1) {
      void* result = &memory[freeChunkIndex];
      zeroOut(memory, amount);
      return result;  
    }
  }
  return NULL;
}

void copyMemory(void* what, void* where, int amount) {
  for (int i = 0; i < amount; i++) {
    *((char*)where + i) = *((char*)what + i);
  }
}

void* reallocate(void* what, int amount) {
  void* newHome = allocate(amount);
  int memorySize = min(getAllocationSize((int)((char*)what - memory)), amount);
  copyMemory(what, newHome, memorySize);
  dispose(what);
  return newHome;
}

void dispose(void* what) {
  int index = (char*)what - memory;
  freeAllocationFromIndex(index);
}
