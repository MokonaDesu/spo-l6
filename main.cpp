#include <stdio.h>

#include "memory.h"

#define TC1_SIZE 63
#define TC3_SIZE 15
#define TC3_DELTA 2

void testScenarioOne() {
  int* someInts = (int*)allocate(TC1_SIZE * sizeof(int));  
  printf("Allocated integer array of %d elements @%x\n", TC1_SIZE, someInts);
  printf("Numbers are: ");
  for(int i = 0; i < TC1_SIZE; i++) 
  {
    someInts[i] = i;
    printf("%d ", someInts[i]);
  }

  dispose(someInts);
  printf("Successfully disposed of that memory.\n");
}

void testScenarioTwo() {
  int* i1 = (int*) allocate(sizeof(int));
  printf("First integer is @%x\n", i1);
  int* i2 = (int*) allocate(sizeof(int));
  printf("Second one is @%x\n", i2);
  dispose(i1);
  int* i3 = (int*) allocate(sizeof(int));
  printf("First one is disposed of.\n");
  printf("And now the third integer is @%x\n", i3);

  dispose(i2);
  dispose(i3);
}

void testScenarioThree() {
  int* someInts = (int*)allocate(TC3_SIZE * sizeof(int));  
  printf("Allocated integer array of %d elements @%x\n", TC3_SIZE, someInts);
  printf("Numbers are: ");
  for(int i = 0; i < TC3_SIZE; i++) 
  {
    someInts[i] = i;
    printf("%d ", someInts[i]);
  }
  printf("\n");
  someInts = (int*)reallocate(someInts, sizeof(int) * (TC3_SIZE + TC3_DELTA));
  printf("Reallocated array to be %d integers @%x\n",TC3_SIZE + TC3_DELTA, someInts);
  printf("Now this array contains: ");
  for(int i = 0; i < TC3_SIZE + TC3_DELTA; i++) {
    printf("%d ", someInts[i]);
  }

  printf("\nReallocating to a smaller array\n");
  someInts = (int*)reallocate(someInts, sizeof(int) * (TC3_SIZE - TC3_DELTA));
  printf("Reallocated array to be %d integers @%x\n",TC3_SIZE - TC3_DELTA, someInts);
  printf("Now this array contains: ");
  for(int i = 0; i < TC3_SIZE - TC3_DELTA; i++) {
    printf("%d ", someInts[i]);
  }
  printf("\n");
  printf("Sucessfully disposed of that memory.\n");
  dispose(someInts);
}

int main(int argc, char** argv) {
  printf("First scenario - array allocation:\n");
  testScenarioOne();
  printf("Second scenario - distinct items & substitution after disposal.\n");
  testScenarioTwo();
  printf("Third scenario - array reallocation\n");
  testScenarioThree();

  return 0;
}
