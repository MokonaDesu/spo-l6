#ifndef MEMORY_H
#define MEMORY_H
//Allocates <int> bytes of memory and returns 
//<void*> to it's first byte.
//You are free to write and read outside the allocated region,
//but such memory is not guaranteed to remain intact. 
void* allocate(int);

//Reallocates the  memory pointed at by the  <void*> pointer
//to be <int> bytes.
//Old memory block is copied to the beggining of the new one.
//Reallocating to smaller amounts of memory is allowed, but
//it may lead to loss of data.
void* reallocate(void*, int);

//Releases an allocatio of memory chunk pointed at by the <void*> pointer.
void  dispose(void*);

//Nullifies n bytes from the beggining of the void pointer. 
void  zeroOut(void*, int);

//Copies <int> bytes of memory from blocks of memory
//pointed at by first and second void* pointers.
void  copyMemory(void*, void*, int);
#endif
